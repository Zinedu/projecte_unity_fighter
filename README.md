Un joc d'estil smash amb diferents mapes i obstscles.

**Controls:**

p1: 
- Atac: Z
- Defensa: X
- Moviment: fletxes
- Combo: ZZCC

p2:
- Atac: Mouse left
- Defensa: Mouse Right
- Moviment: Numapad 5, 1, i 3
- Combo: M. Left, M. Left, M. Middle, M. Middle

Scripts:

**P1_Movement:**

Controla el moviment, fisiques i animacions del jugador 1 i llegeix combos a partir d'un arraylist que llegeix cada input realitzat, l'arraylist es purga a si mateix despres de fer un combo per no colapsar la memoria.

**P2_Movement:**

Identic a P1_Moviment, pero en diferents referencies i controls per al segon jugador.

**BoundsScript:**

Utilitzat per el tileset de "Bounds" en mapes on el jugador pot caure, retorna al jugador a la posició 0,0 epr evitar que caigui infinitament i la dona un cop.

**AttackScript:**

Script de control de la hitbox d'atac, un cop s'activa, executa la seva animació i es desactiva automaticament.

El triggercollider mira l'estat del jugador i executa un event si col·lisiona amb un jugador que no sigui el seu pare i no s'estigui defensant, també controla les fisiques del cop al jugador si contacta.

**FireballScript:**

Extremadament similar al d'atac, pero control·lant la direcció de la bola de foc, la bola de foc s'autodestrueix si col·lisiona amb les bounds del nivell.

**HazardScript:**

Script per a la capa de hazard del mapa 3, assigna mal al jugador que ha fet col·lisió i el fa rebotar cap a l'aire.

**HP:**

Té els valors de vida del jugador i controla el grafic de la barra de vida escoltant als events de FireballScript, HazardScript i AttackScript, la barra de vida està dividida en segments que es van perdent a mesura que el jugador rep cops.

També canvia l'escena a la de victoria corresponent quan un jugador arriva a 0 de vida.

