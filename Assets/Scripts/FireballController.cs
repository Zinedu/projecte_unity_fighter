﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballController : MonoBehaviour
{
    public GameObject owner;
    public int direction = 0;
    public delegate void hit(string player);
    public static event hit OnHit;
    // Start is called before the first frame update
    void Start()
    {

        switch (direction)
        {
            case 0:
                this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-5, 0);
                this.gameObject.GetComponent<Transform>().rotation = Quaternion.Euler(0, 0, 0);
                break;
            case 1:
                this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(5, 0);
                this.gameObject.GetComponent<Transform>().rotation = Quaternion.Euler(0, 180f, 0);
                break;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && collision.gameObject != owner)
        {
            string targetName = collision.gameObject.name;
            switch (targetName)
            {
                case "p1":
                    if (!collision.gameObject.GetComponent<P1_movement>().invuln)
                    {
                        if (OnHit != null)
                        {
                            OnHit(collision.gameObject.name);
                            if (this.GetComponentInParent<Transform>().position.x < collision.gameObject.GetComponent<Transform>().position.x)
                            {
                                collision.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(400, 200));
                            }
                            if (this.GetComponentInParent<Transform>().position.x > collision.gameObject.GetComponent<Transform>().position.x)
                            {
                                collision.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-400, 200));
                            }
                        }
                        collision.gameObject.GetComponent<P1_movement>().invuln = true;
                    }
                    collision.gameObject.GetComponent<P1_movement>().grounded = false;
                    break;
                case "p2":
                    if (!collision.gameObject.GetComponent<P2_movement>().invuln)
                    {
                        if (OnHit != null)
                        {
                            OnHit(collision.gameObject.name);
                            if (this.GetComponentInParent<Transform>().position.x < collision.gameObject.GetComponent<Transform>().position.x)
                            {
                                collision.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(400, 200));
                            }
                            if (this.GetComponentInParent<Transform>().position.x > collision.gameObject.GetComponent<Transform>().position.x)
                            {
                                collision.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-400, 200));
                            }
                        }
                        collision.gameObject.GetComponent<P2_movement>().invuln = true;
                        collision.gameObject.GetComponent<P2_movement>().grounded = false;
                    }
                    break;
            }
        }
        if (collision.gameObject.tag == "Bound")
        {
            GameObject.Destroy(this.gameObject);
        }
    }
}
