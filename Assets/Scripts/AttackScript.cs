﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackScript : MonoBehaviour
{
    public delegate void hit(string player);
    public static event hit OnHit;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    private void OnEnable()
    {
        StartCoroutine(WaitForAnim());
    }

    // Update is called once per frame
    void Update()
    {
        
        
        
    }

    IEnumerator WaitForAnim()
    {
        yield return new WaitForSeconds(this.gameObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length);
        this.gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!this.gameObject.GetComponent<Transform>().IsChildOf(collision.gameObject.GetComponent<Transform>()) && collision.gameObject.tag == "Player")
        {
            string targetName = collision.gameObject.name;
            switch (targetName)
            {
                case "p1":
                    if (!collision.gameObject.GetComponent<P1_movement>().invuln)
                    {
                        if (OnHit != null)
                        {
                            OnHit(collision.gameObject.name);
                            if (this.GetComponentInParent<Transform>().position.x < collision.gameObject.GetComponent<Transform>().position.x)
                            {
                                collision.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(400, 200));
                            }
                            if (this.GetComponentInParent<Transform>().position.x > collision.gameObject.GetComponent<Transform>().position.x)
                            {
                                collision.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-400, 200));
                            }
                        }
                        collision.gameObject.GetComponent<P1_movement>().invuln = true;
                    }
                    collision.gameObject.GetComponent<P1_movement>().grounded = false;
                    break;
                case "p2":
                    if (!collision.gameObject.GetComponent<P2_movement>().invuln)
                    {
                        if (OnHit != null)
                        {
                            OnHit(collision.gameObject.name);
                            if (this.GetComponentInParent<Transform>().position.x < collision.gameObject.GetComponent<Transform>().position.x)
                            {
                                collision.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(400, 200));
                            }
                            if (this.GetComponentInParent<Transform>().position.x > collision.gameObject.GetComponent<Transform>().position.x)
                            {
                                collision.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-400, 200));
                            }
                        }
                        collision.gameObject.GetComponent<P2_movement>().invuln = true;
                        collision.gameObject.GetComponent<P2_movement>().grounded = false;
                    }
                    break;
            }
        }
            
    }
}
