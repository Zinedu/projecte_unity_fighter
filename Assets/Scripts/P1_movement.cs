﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;
using UnityEngine.SceneManagement;

public class P1_movement : MonoBehaviour
{
    public float speedX = 0;
    public float speedY = 0;
    public bool grounded = true;
    public bool doubleJump = true;
    public bool shielding = false;
    public bool invuln = false;
    float maxAccel = 5;
    public Animator anims;
    bool rightFace = false;
    GameObject strike;
    GameObject shield;
    ArrayList inputList = new ArrayList();
    bool inputTimerPurge = false;
    public GameObject part;
    // Start is called before the first frame update
    void Start()
    {
        anims = this.gameObject.GetComponent<Animator>();
        anims.SetBool("Grounded", true);
        strike = GameObject.Find("p1_strike");
        strike.SetActive(false);
        shield = GameObject.Find("p1_shield");
        shield.SetActive(false);
        part = GameObject.Find("p1_part");
    }

    // Update is called once per frame
    void Update()
    {
        speedX = this.gameObject.GetComponent<Rigidbody2D>().velocity.x;
        speedY = this.gameObject.GetComponent<Rigidbody2D>().velocity.y;
        anims.SetFloat("Velocity", speedX);
        if (SceneManager.GetActiveScene().name != "P1_Win" && SceneManager.GetActiveScene().name != "P2_Win")
        {
            if (speedX != 0)
            {
                if (speedX > 0)
                {
                    rightFace = true;
                }
                else
                {
                    rightFace = false;
                }
            }

            if (rightFace)
            {
                this.gameObject.GetComponent<Transform>().rotation = Quaternion.Euler(0, 0, 0);
            }
            else
            {
                this.gameObject.GetComponent<Transform>().rotation = Quaternion.Euler(0, 180f, 0);
            }

            if (Input.GetKey(KeyCode.RightArrow))
            {
                this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(maxAccel, speedY);
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-maxAccel, speedY);
            }
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                if (grounded)
                {
                    this.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 400));
                    grounded = false;
                    anims.SetBool("Grounded", false);
                }
                else
                {
                    if (doubleJump)
                    {
                        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(speedX, 0);
                        this.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 300));
                        doubleJump = false;
                    }
                }
            }
            if (!Input.anyKey)
            {
                if (!invuln)
                {
                    this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(speedX / 2, speedY);
                }
            }
            if (Input.GetKeyDown(KeyCode.Z) && !shielding)
            {
                StartCoroutine("PlayerInputAttack");
            }
            if (Input.GetKey(KeyCode.X))
            {
                shielding = true;
                this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, speedY);

            }
            if (Input.GetKeyUp(KeyCode.X))
            {
                shielding = false;
            }

            if (shielding && !shield.activeSelf)
            {
                shield.SetActive(true);
            }
            else
            {
                if (!shielding)
                {
                    shield.SetActive(false);
                }
            }
            if (invuln)
            {
                StartCoroutine(InvulnTimer());
            }
            StartCoroutine("ReadInput");
            StartCoroutine("InputComboResults");
        }
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Ground")
        {
            grounded = true;
            anims.SetBool("Grounded", true);
            doubleJump = true;
        }
    }

    void PlayerInputAttack()
    {
        if (!strike.activeSelf)
        {
            strike.SetActive(true);
        }
        return;
        
    }

    IEnumerator InvulnTimer()
    {
        this.gameObject.GetComponent<Renderer>().material.color = new Color(1, 1, 1, 0.2f);
        yield return new WaitForSeconds(1);
        this.gameObject.GetComponent<Renderer>().material.color = new Color(1, 1, 1, 1f);
        invuln = false;
    }

    void ReadInput()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            inputList.Add("R");
            if (inputTimerPurge)
            {
                inputTimerPurge = false;
            }
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            inputList.Add("L");
            if (inputTimerPurge)
            {
                inputTimerPurge = false;
            }
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            inputList.Add("Z");
            if (inputTimerPurge)
            {
                inputTimerPurge = false;
            }
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            inputList.Add("C");
            if (inputTimerPurge)
            {
                inputTimerPurge = false;
            }
        }
        
    }

    void InputComboResults()
    {
        Debug.Log(inputList.Count);
        string combo = "";
        if (inputList.Count > 5)
        {
            combo = combo + inputList[inputList.Count - 4];
            combo = combo + inputList[inputList.Count - 3];
            combo = combo + inputList[inputList.Count - 2];
            combo = combo + inputList[inputList.Count - 1];
            
        }
        Debug.Log(combo.ToString());
        switch (combo)
        {
            case "ZZCC":
                GameObject fireball = (GameObject)Instantiate(Resources.Load("fireball"), this.gameObject.transform.position, Quaternion.identity);
                fireball.GetComponent<FireballController>().owner = this.gameObject;
                switch (this.rightFace)
                {
                    case true:
                        fireball.GetComponent<FireballController>().direction = 1;
                        break;
                    case false:
                        fireball.GetComponent<FireballController>().direction = 0;
                        break;
                }
                inputList.RemoveRange(0, inputList.Count - 1);
                break;
        }
    }

    IEnumerator TimerCombo()
    {
        inputTimerPurge = true;
        yield return new WaitForSeconds(2);
        if (inputTimerPurge) { inputList.Clear(); }
    }

}
