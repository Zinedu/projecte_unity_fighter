﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundsScript : MonoBehaviour
{
    public delegate void bounds(string player);
    public static event bounds OnBoundsHit;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            if (OnBoundsHit != null)
            {
                OnBoundsHit(collision.gameObject.name);
                collision.gameObject.GetComponent<Transform>().position = new Vector2(0, 0);
            }
        }
    }

}
