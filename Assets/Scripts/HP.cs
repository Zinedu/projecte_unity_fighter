﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HP : MonoBehaviour
{
    public string id;
    public int hp = 110;
    public Image hpBar;
    GameObject p1;
    GameObject p2;
    GameObject p1_HealthBar;
    GameObject p2_HealthBar;
    Vector2 originalHPSize;
    // Start is called before the first frame update
    void Start()
    {
        id = this.gameObject.name;
        AttackScript.OnHit += Hit;
        BoundsScript.OnBoundsHit += Hit;
        FireballController.OnHit += Hit;
        HazardScript.OnHit += Hit;
        p1 = GameObject.Find("p1");
        p2 = GameObject.Find("p2");
        p1_HealthBar = GameObject.Find("p1_hp_bar");
        p2_HealthBar = GameObject.Find("p2_hp_bar");
        this.gameObject.GetComponent<Text>().text = "";
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void Hit(string p)
    {

        switch(p)
        {
            case "p1": 
                if(id == "p1_hp")
                {
                    hp -= 10;
                    p1.GetComponent<AudioSource>().PlayOneShot((AudioClip)Resources.Load("hit"));
                    p1.GetComponent<P1_movement>().part.GetComponent<ParticleSystem>().Play();
                    switch (hp)
                    {
                        case 90:
                            GameObject.Destroy(GameObject.Find("p1_segment10").gameObject);
                            break;
                        case 80:
                            GameObject.Destroy(GameObject.Find("p1_segment9").gameObject);
                            break;
                        case 70:
                            GameObject.Destroy(GameObject.Find("p1_segment8").gameObject);
                            break;
                        case 60:
                            GameObject.Destroy(GameObject.Find("p1_segment7").gameObject);
                            break;
                        case 50:
                            GameObject.Destroy(GameObject.Find("p1_segment6").gameObject);
                            break;
                        case 40:
                            GameObject.Destroy(GameObject.Find("p1_segment5").gameObject);
                            break;
                        case 30:
                            GameObject.Destroy(GameObject.Find("p1_segment4").gameObject);
                            break;
                        case 20:
                            GameObject.Destroy(GameObject.Find("p1_segment3").gameObject);
                            break;
                        case 10:
                            GameObject.Destroy(GameObject.Find("p1_segment2").gameObject);
                            break;
                    }
                    if (hp == 0)
                    {
                        SceneManager.LoadScene("P2_Win");
                    }
                }
                break;
            case "p2":
                if(id == "p2_hp")
                {
                    hp -= 10;
                    p2.GetComponent<AudioSource>().PlayOneShot((AudioClip)Resources.Load("hit"));
                    p2.GetComponent<P2_movement>().part.GetComponent<ParticleSystem>().Play();
                    switch (hp)
                    {
                        case 90:
                            GameObject.Destroy(GameObject.Find("p2_segment10").gameObject);
                            break;
                        case 80:
                            GameObject.Destroy(GameObject.Find("p2_segment9").gameObject);
                            break;
                        case 70:
                            GameObject.Destroy(GameObject.Find("p2_segment8").gameObject);
                            break;
                        case 60:
                            GameObject.Destroy(GameObject.Find("p2_segment7").gameObject);
                            break;
                        case 50:
                            GameObject.Destroy(GameObject.Find("p2_segment6").gameObject);
                            break;
                        case 40:
                            GameObject.Destroy(GameObject.Find("p2_segment5").gameObject);
                            break;
                        case 30:
                            GameObject.Destroy(GameObject.Find("p2_segment4").gameObject);
                            break;
                        case 20:
                            GameObject.Destroy(GameObject.Find("p2_segment3").gameObject);
                            break;
                        case 10:
                            GameObject.Destroy(GameObject.Find("p2_segment2").gameObject);
                            break;
                    }
                    if (hp == 0)
                    {
                        SceneManager.LoadScene("P1_Win");
                    }
                }
                break;
        }
    }

}
