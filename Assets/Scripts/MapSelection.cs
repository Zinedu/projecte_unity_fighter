﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MapSelection : MonoBehaviour
{
    Button s;

    // Start is called before the first frame update
    void Start()
    {
        s = this.gameObject.GetComponent<Button>();
        s.onClick.AddListener(StageChange);
    }

    // Update is called once per frame
    void Update()
    {
                
    }

    void StageChange() {
        switch (this.gameObject.name)
        {
            case "s1":
                SceneManager.LoadScene("Map_1");
                break;
            case "s2":
                SceneManager.LoadScene("Map_2");
                break;
            case "s3":
                SceneManager.LoadScene("Map_3");
                break;
        }
    }
    
}
