﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardScript : MonoBehaviour
{
    public delegate void hit(string player);
    public static event hit OnHit;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        string targetName = collision.gameObject.name;
        switch (targetName)
        {
            case "p1":
                if (!collision.gameObject.GetComponent<P1_movement>().invuln)
                {
                    if (OnHit != null)
                    {
                        OnHit(collision.gameObject.name);
                        if (this.GetComponentInParent<Transform>().position.x < collision.gameObject.GetComponent<Transform>().position.x)
                        {
                            collision.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 200));
                        }
                        if (this.GetComponentInParent<Transform>().position.x > collision.gameObject.GetComponent<Transform>().position.x)
                        {
                            collision.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 200));
                        }
                    }
                    collision.gameObject.GetComponent<P1_movement>().invuln = true;
                }
                collision.gameObject.GetComponent<P1_movement>().grounded = false;
                break;
            case "p2":
                if (!collision.gameObject.GetComponent<P2_movement>().invuln)
                {
                    if (OnHit != null)
                    {
                        OnHit(collision.gameObject.name);
                        if (this.GetComponentInParent<Transform>().position.x < collision.gameObject.GetComponent<Transform>().position.x)
                        {
                            collision.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 200));
                        }
                        if (this.GetComponentInParent<Transform>().position.x > collision.gameObject.GetComponent<Transform>().position.x)
                        {
                            collision.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 200));
                        }
                    }
                    collision.gameObject.GetComponent<P2_movement>().invuln = true;
                    collision.gameObject.GetComponent<P2_movement>().grounded = false;
                }
                break;
        }
    }
}
